<?php
namespace App\Controller;


use App\Service\StripeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

/**
 * Created by PhpStorm.
 * User: dell
 * Date: 19.2.19
 * Time: 18.55
 */

class PaymentController extends AbstractController {

    public function index(StripeService $stripeService, Request $request, ParameterBagInterface $parameterBag){
        $form = $this->createFormBuilder(null,['allow_extra_fields' => true,'attr' => ['id' => 'payment-form']])
            ->add('submit', SubmitType::class)
            ->add('stripeToken', HiddenType::class)
            ->getForm();

        if ($request->isMethod('POST')) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                if ($stripeService->demand($parameterBag->get('stripe.amount'), $form->get('stripeToken')->getData())) {
                    $this->get('session')->getFlashBag()->add('success', "Payment completed.");
                } else {
                    $this->get('session')->getFlashBag()->add('warning', "Payment failed");
                }
            }
        }

        return $this->render('payment/index.html.twig',  [
            'form' => $form->createView(),
            'stripePublicKey' => $parameterBag->get('stripe.api_key_public')
        ]);
    }
}