<?php

namespace App\Service;
use Stripe\ApiRequestor;
use Stripe\ApiResource;
use Stripe\Charge;
use Stripe\Error\Base;
use Stripe\Stripe;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Created by PhpStorm.
 * User: dell
 * Date: 19.2.19
 * Time: 21.26
 */

class StripeService {

    const LOG_FILE = 'logs.txt';

    private  $currency, $fileSystem;

    public function __construct(ParameterBagInterface $parameterBag, Filesystem $filesystem) {
        Stripe::setApiKey($parameterBag->get('stripe.api_key_secret'));
        $this->currency = $parameterBag->get('stripe.currency');
        $this->fileSystem = $filesystem;
    }

    public function demand($amount, $token) {
        try {
            $charge = Charge::create(['amount' => $amount, 'currency' => $this->currency, 'source' => $token]);
        } catch (Base $exception) {
            return false;
        }

        $this->logTransaction($charge);
        return true;
    }

    /**
     * @param Charge $charge
     */
    private function logTransaction(ApiResource $charge) {
        $this->fileSystem->appendToFile(self::LOG_FILE, json_encode($charge->values()));
    }
}